package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	//商品定義ファイル名（追加）
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";
	//商品別集計ファイル名（追加）
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "定義ファイルのフォーマットが不正です";
	private static final String FILE_NOT_SERIAL_NUMBER = "売り上げファイル名が連番になっていません";
	private static final String FILE_AMOUNT_OVER = "合計金額が10桁を超えました";
	private static final String FILE_INVALID_BRANCHCODE = "の支店コードが不正です";
	private static final String FILE_INVALID_COMMODITY_CODE = "の商品コードが不正です";
	private static final String FILE_FORMAT_ERROR = "のフォーマットが不正です";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		//エラー処理３(1)コマンドライン引数が設定されているか
		if(args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		//商品コードと商品名を保持するMap(追加)
		Map<String,String> commodityNames = new HashMap<>();
		//商品コードと売り上げ金額を保持するMap(追加)
		Map<String,Long> commoditySales = new HashMap<>();

		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, "^[0-9]{3}$", "支店")) {
			return;
		}
		// 商品定義ファイル読み込み処理(追加)
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales, "^[A-Za-z0-9]{8}$", "商品")) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		for(int i = 0; i < files.length ; i++) {
			if(files[i].isFile() && files[i].getName().matches("^[0-9]{8}.rcd$")) {
				//売り上げファイルのリスト
				rcdFiles.add(files[i]);
			}
		}
		//エラー処理２-1 連番になっているか
		//連番ソート
		Collections.sort(rcdFiles);
		for (int i = 0; i <rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));
			if((latter - former) != 1) {
				System.out.println(FILE_NOT_SERIAL_NUMBER);
				return ;
			}
		}
		for(int i = 0 ; i < rcdFiles.size(); i++) {
			BufferedReader br = null;

			try {
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);
				ArrayList<String> array = new ArrayList<String>();
				String line;
				// 一行ずつ読み込む

				while((line = br.readLine()) != null) {
					//売り上げファイル中身をリスト化
					array.add(line);

				}
				//エラー処理２(3) 売り上げファイルが3行になっているか(2→3に修正)
				if(array.size() != 3) {
					System.out.println(rcdFiles.get(i) + FILE_FORMAT_ERROR);
					return;
				}
				//エラー処理２(2) Mapのkeyが存在するか i番目の支店コードが不正です
				if(! branchNames.containsKey(array.get(0))) {
					System.out.println(rcdFiles.get(i) + FILE_INVALID_BRANCHCODE);
					return;
				}

				//商品定義エラー追加
				if(! commodityNames.containsKey(array.get(1))) {
					System.out.println(rcdFiles.get(i) + FILE_INVALID_COMMODITY_CODE);
					return;
				}

				//エラー処理３(3) 金額が数字で構成されているか
				if(! array.get(2).matches("^[0-9]+$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				long fileSale = Long.parseLong(array.get(2));
				//支店売り上げ金額の合計
				Long saleAmount = branchSales.get(array.get(0)) + fileSale;
				//商品別売り上げ金額合計(追加)
				Long commodityAmount = commoditySales.get(array.get(1)) + fileSale;
				//エラー処理2-1(1)金額が11桁以上か
				if(saleAmount >= 10000000000L) {
					System.out.println(FILE_AMOUNT_OVER);
					return;
				}
				if(commodityAmount >= 10000000000L) {
					System.out.println(FILE_AMOUNT_OVER);//(商品定義エラー追加)
					return;
				}
				//マップに支店別合計金額情報を挿入
				branchSales.put(array.get(0),saleAmount);
				//マップに商品別売り上げ合計を挿入//(追加)
				commoditySales.put(array.get(1),commodityAmount);

			}catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				// ファイルを開いている場合
				if(br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}
		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}
		// 商品別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}
	}
	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales, String regexp, String definition) {
		BufferedReader br = null;

		try {
			//（課題ファイルのパス,(branchlist,commoditylist)）
			File file = new File(path, fileName);
			//エラー処理1 ファイルが存在するか
			if ( ! file.exists()) {
				//支店・商品どちらもエラー表示
				System.out.println(definition + FILE_NOT_EXIST);
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {
				// ※ここの読み込み処理を変更してください。(処理内容1-2)
				String[] items = line.split(",");	//,で区切る
				//エラー処理１(2) 3桁と,で構成されているか
				//正規表現が合っているか
				if ((items.length != 2) ||(! items[0].matches(regexp))){
					//支店・商品どちらもエラー表示
					System.out.println(definition + FILE_INVALID_FORMAT);
					return false;
				}
				//支店コード,支店名（商品コード,商品名）
				names.put(items[0], items[1]);
				//支店コード,合計金額（商品コード,合計金額）
				sales.put(items[0], 0L);
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)

		File file = new File(path, fileName);
		BufferedWriter bw = null;

		try {
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);
			//キーを使ってバリューを書き出す
			for(String key : sales.keySet()) {
				bw.write(key + "," + names.get(key) + "," + sales.get(key));
				bw.newLine();
			}

		}catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		}finally {
			if(bw != null) {
				try {
					bw.close();
				}catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}

		return true;
	}
}
